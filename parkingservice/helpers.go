package parkingservice

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func UnmarshalJsonResponse(response *http.Response, t interface{}) error {
	defer response.Body.Close()
	var err error
	var content []byte
	if content, err = ioutil.ReadAll(response.Body); err != nil {
		return err
	}

	if err = json.Unmarshal(content, t); err != nil {
		return err
	}

	return nil
}
