package parkingservice

import "net/http"

var (
	apiUrl = "http://ucn-parking.herokuapp.com/places.json"
)

// Get newest parking lot information from API service
func GetParkingLots() ([]ParkingLot, error) {
	var err error
	var response *http.Response
	if response, err = http.Get(apiUrl); err != nil {
		return nil, err
	} else {
		parkingLots := make([]ParkingLot, 0)
		if err = UnmarshalJsonResponse(response, &parkingLots); err != nil {
			return nil, err
		}

		return parkingLots, nil
	}
}
