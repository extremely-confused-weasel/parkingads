package parkingservice

type ParkingLot struct {
	Name            string  `json:"name"`
	IsOpen          byte    `json:"is_open,string"`
	IsPaymentActive byte    `json:"is_payment_active,string"`
	StatusParkPlace byte    `json:"status_park_place,string"`
	Longitude       float64 `json:",string"`
	Latitude        float64 `json:",string"`
	MaxCount        int16   `json:"max_count,string"`
	FreeCount       int16   `json:"free_count,string"`
}
