package confusedwebserver

import (
	"fmt"
	"io/ioutil"
	"net/http"
)
//server starts
func StartServer() error {
	http.HandleFunc("/assets/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "confusedwebserver/"+r.URL.Path[1:])
	})

	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/api/sendemail", sendEmail)

	fmt.Println("Server started. Listening on port 5000")
	if err := http.ListenAndServe(":5000", nil); err != nil {
		return err
	}

	return nil
}

func handleIndex(res http.ResponseWriter, req *http.Request) {
	content, err := ioutil.ReadFile("confusedwebserver/index.html")
	if err != nil {
		fmt.Fprintf(res, "<html><head></head><body><p>An error occurred</p></body></html>")
		fmt.Println("Error: ", err)
	} else {
		fmt.Fprintf(res, string(content))
	}
}
