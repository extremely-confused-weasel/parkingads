package confusedwebserver

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func sendEmail(res http.ResponseWriter, req *http.Request) {
	user := User{}

	defer req.Body.Close()
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println("Error:", err)
	} else {
		if err = json.Unmarshal(body, &user); err != nil {
			log.Println("Error:", err)
		} else {
			log.Printf("Parsed user: email: %s - location: Lat=%f Long=%f", user.Email, user.Location.Lat, user.Location.Long)
		}
	}

	var status string
	if err == nil {
		status = "{\"status\":\"success\"}"
	} else {
		status = "{\"status\":\"failed\"}"
	}
	data := []byte(status)

	res.Header().Set("Content-Type", "application/json; charset=utf-8")
	res.Write(data)
}
