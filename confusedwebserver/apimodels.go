package confusedwebserver

type GeoLocation struct {
	Lat  float64
	Long float64
}

type User struct {
	Email    string
	Location *GeoLocation
}
