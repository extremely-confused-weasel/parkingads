function IndexViewModel() {
    var self = this;

    self.email = ko.observable();
    self.agreeTos = ko.observable(false);
    self.sendEmail = function() {
        if(!self.agreeTos()) {
            alert('Please agree');
        }
        else {
            self.hideAlerts();
            if(self.email() && self.email().length > 0) {
                var obj = {
                    "Email": self.email(),
                    "Location": {
                        "Lat": 52.0000,
                        "Long": 53.0000,
                    }
                };

                $.ajax({
                    url: "/api/sendemail",
                    type: "post",
                    data: JSON.stringify(obj),
                    success: function(response){
                        console.log(response);
                        self.email("");
                        self.agreeTos(false);
                        $(".alerts #alert-success").show();
                        setTimeout(function () {
                            self.hideAlerts();
                        }, 10000);
                    },
                    error:function(response){
                        console.log(response);
                        $(".alerts #alert-error").show();
                    }
                });
            } else {
                alert('Please enter your email');
            }
        }
    };

    self.hideAlerts = function () {
        $(".alerts #alert-error").hide();
        $(".alerts #alert-success").hide();
    };
}
