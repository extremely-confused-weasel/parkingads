ParkingAds
=========================
[![Build 
Status](https://drone.io/bitbucket.org/extremely-confused-weasel/parkingads/status.png)](https://drone.io/bitbucket.org/extremely-confused-weasel/parkingads/latest)

## Purpose
The purpose of ParkingAds is to exercise the knowledge and skills students of Development of Large Systems gain during the lectures. Time for implementing the solution will be available as part of the lectures.

## Description
ParkingAds is a relatively simple idea: Users send their location and email address from a mobile device to the ParkingAds service, that will generate a map with indications of nearby parking lots and how many available spaces each have. This map will be returned to the user in an email that also contains embedded ads fetched from an ad service.

The ParkingAds server can access parking lot information for Aalborg from an external service provided by Aalborg Kommune. The service is described in other documents provided.

## Challenges
ParkingAds must be able to handle 1000 requests per minute.
The ParkingService from Aalborg Kommune is restricted to one request per minute per account.

## Dependencies
- gopm: https://github.com/gpmgo/gopm
- memcached: http://memcached.org/

## Authors
- Chris Tindbæk
- Morten Klim Sørensen
