package main

import (
	"fmt"
	"log"

	webserver "ParkingAds/confusedwebserver"
)

func main() {
	cws := make(chan bool)
	go func() {
		if err := webserver.StartServer(); err != nil {
			log.Fatal("Error: ", err)
		}
		cws <- true
	}()

	fmt.Println("Waiting...")
	<-cws
}
