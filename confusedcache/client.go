package confusedcache

import (
	"errors"

	memcache "github.com/bradfitz/gomemcache/memcache"
)

var (
	cache  *memcache.Client
	Config *MemcacheConfig
)

func MakeRequest(closure func() error) error {
	if cache == nil {
		if err := Connect(); err != nil {
			return err
		}
	}

	return closure()
}

func Connect() error {
	if cache != nil {
		return errors.New("Connection is allready open")
	}
	if Config == nil {
		return errors.New("No configuration specified")
	}

	cache = memcache.New(Config.Hosts...)

	return nil
}

func AddItem(item *memcache.Item) error {
	closure := func() error {
		var err error

		if item, _ := cache.Get(item.Key); item != nil {
			return errors.New("Item allready exists in the cache")
		}

		if err = cache.Set(item); err != nil {
			return err
		}

		return nil
	}

	return MakeRequest(closure)
}

func ReplaceItem(item *memcache.Item) error {
	closure := func() error {
		var err error

		if err = cache.Replace(item); err != nil {
			return err
		}

		return nil
	}

	return MakeRequest(closure)
}

func GetItem(key string) (*memcache.Item, error) {
	var item *memcache.Item
	var err error

	closure := func() error {
		if item, err = cache.Get(key); err != nil {
			return err
		}

		return nil
	}

	return item, MakeRequest(closure)
}

func DeleteItem(key string) error {
	closure := func() error {
		if err := cache.Delete(key); err != nil {
			return err
		}

		return nil
	}

	return MakeRequest(closure)

}

func FlushAll() error {
	closure := func() error {
		if err := cache.FlushAll(); err == nil {
			return errors.New("PANIC! The cache is flushable!")
		}

		return nil
	}

	return MakeRequest(closure)
}
