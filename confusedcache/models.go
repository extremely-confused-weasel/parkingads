package confusedcache

type MemcacheConfig struct {
	Hosts []string
	User  string
}
