package confusedcache

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"testing"

	memcache "github.com/bradfitz/gomemcache/memcache"
)

var (
	itemKeys = []string{
		"Test",
	}
	itemValues = []string{
		"Really important content",
		"Not so important content",
	}
)

func TestParseConfig(t *testing.T) {
	fmt.Println("### TestParseConfig ###")

	if config, err := readConfiguration(); err != nil {
		t.Error("Could not parse config: ", err)
	} else {
		fmt.Printf("Successfully parsed config: %v \n", config)
		Config = &config.Memcache
	}
}

func TestAddItem(t *testing.T) {
	fmt.Println("### TestAddItem ###")
	var err error
	var item *memcache.Item

	item = &memcache.Item{Key: itemKeys[0], Value: []byte(itemValues[1])}
	if err = AddItem(item); err != nil {
		t.Error("Unable to add item to the cache: ", err)
	} else {
		fmt.Println("Successfully added an item to the cache")
	}
}

// Get a cached ParkingLot item
// Pre-condition.: a cached ParkingLot item exists in the cache
// Post-condition: the cached item is retrieved from the cache
func TestGetItem(t *testing.T) {
	fmt.Println("### TestGetItem ###")
	var err error
	var item *memcache.Item

	if item, err = GetItem("Test"); err != nil {
		t.Error("Unable to retrieve item from cache: ", err)
	} else {
		fmt.Printf("Cached Item: %s\n", item)
	}
}

func TestReplaceItem(t *testing.T) {
	fmt.Println("### TestReplaceItem ###")

	item := &memcache.Item{Key: itemKeys[0], Value: []byte(itemValues[0])}
	if err := ReplaceItem(item); err != nil {
		t.Error("Unable to replace item: ", err)
	} else {
		fmt.Println("Successfully replaced item")
	}
}

func TestDeleteItem(t *testing.T) {
	fmt.Println("### TestDeleteItem ###")

	if err := DeleteItem("Test"); err != nil {
		t.Error("Unable to delete the item: ", err)
	} else {
		fmt.Println("Successfully deleted the item")
	}
}

func TestFlushAll(t *testing.T) {
	fmt.Println("### TestFlushAll ###")

	if err := FlushAll(); err != nil {
		t.Error("Were able to flush cache: ", err)
	} else {
		fmt.Println("Great! Was unable to flush cache")
	}
}

type jsonObj struct {
	Configuration Configuration `json: configuration`
}

type Configuration struct {
	Memcache MemcacheConfig `json: memcache`
}

func readConfiguration() (*Configuration, error) {
	file, err := ioutil.ReadFile("../config.json")
	if err != nil {
		return nil, err
	}

	obj := jsonObj{}
	if err = json.Unmarshal(file, &obj); err != nil {
		return nil, err
	}

	return &obj.Configuration, nil
}
